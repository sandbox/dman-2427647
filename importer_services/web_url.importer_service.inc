<?php
/**
 * @file
 * An importer service plugin definition.
 */

/**
 * Declare this plugin.
 */
$plugin = array(
  'provider'             => 'Web URL',
  'name'                 => 'Web URL',
  'id'                   => 'web_url',
  'description'          => '
    Enter the location of a remote resource that returns one of the supported formats.
  ',
  // Define the name of the form function that returns service-specific UI.
  'import_form_callback' => 'importer_ui_web_url_form',
  // The function name that will return the raw data - really easy this one!
  'request_callback'     => 'file_get_contents',
);


/**
 * A subform for entering the URL and format to be imported.
 *
 * Callback named in our hook_importer_ui_services
 *
 * $config will include the name of a callback we can use to retrieve the
 * supported formats
 *
 * @implements HOOK_SERVICE_form().
 */
function importer_ui_web_url_form(&$form_state, $service_info, $config) {
  // I need to be sure that this inc always gets loaded if using this form.
  // The automatic includes are failing.
  form_load_include($form_state, 'inc', 'importer_ui', 'importer_services/' . pathinfo(__FILE__, PATHINFO_FILENAME));

  $service_id = $service_info['id'];
  $our_form_values = @$form_state['values'][$service_id];

  $formats_info = $config['formats_callback']();
  $formats_options = $config['formats_callback']('options');

  $form = array(
    '#type'  => 'fieldset',
    '#title' => $service_info['name'],
  );
  $form['url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('URL to import from'),
    '#description'   => t('
      Enter the URL of a file or web service containing the data.
    '),
    '#default_value' => isset($our_form_values['url']) ? $our_form_values['url'] : variable_get('importer_ui_url', ''),
    '#size'          => 128,
  );

  $form['format'] = array(
    '#type'          => 'select',
    '#title'         => t('Format of file'),
    '#default_value' => isset($our_form_values['format']) ? $our_form_values['format'] : variable_get('importer_ui_format', ''),
    '#options'       => $formats_options,
  );
  // Show additional optional descriptive text under the selected format.
  // Only reveal when the appropriate format is selected.
  foreach ($formats_info as $format_key => $format_info) {
    if (!empty($format_info['description'])) {
      $form['format_description'][$format_key] = array(
        '#type'       => 'item',
        'description' => array(
          '#markup' => $format_info['description'],
        ),
        '#states'     => array(
          'visible' => array(
            ':input[name="' . $service_id . '[format]"]' => array('value' => $format_key),
          ),
        ),
      );
    }
  }

  $form['load_url'] = array(
    '#type'     => 'submit',
    '#value'    => t('Load URL'),
    '#validate' => array(
      'importer_ui_web_url_form_validate',
      'importer_ui_import_form_validate'
    ),
    '#submit'   => array('importer_ui_web_url_form_submit'),
    '#id'       => 'web_url',
  );

  return $form;
}


/**
 * Submit handler triggered when 'load file' is clicked.
 *
 * Loads and parses the file, stores results into form memory.
 *
 * Implements HOOK_SERVICE_form_submit()
 */
function importer_ui_web_url_form_submit($form, &$form_state) {
  // Load the data and parse it, store results in form state.
  $form_state['rebuild'] = TRUE;
  // $config was stored for us at form create time.
  $config = $form_state['importer_ui_config'];
  // The clicked button defines the service id. eg 'file_path'
  $service_id = $form_state['clicked_button']['#id'];
  // We are part of a bigger form, and those are all the $form_values.
  $form_values = @$form_state['input'];
  // And the service ID defines which part of the form we read and process.
  $our_form_values = @$form_values[$service_id];
  $strings = array(
    '!file_path'  => l($our_form_values['url'], $our_form_values['url']),
    '@service_id' => $service_id,
  );

  // Early exits here is anything goes wrong.
  $file_content = file_get_contents($our_form_values['url']);
  if (empty($file_content)) {
    drupal_set_message(t("Failed to retrieve content from URL !file_path", $strings), 'error');
    return;
  }

  if (!function_exists($config['formats_callback'])) {
    drupal_set_message(t("The service definition does not return a 'formats_callback' function. Inspect the API docs for the @service_id", $strings), 'error');
    return;
  }
  $formats_info = $config['formats_callback']();
  $format_info = $formats_info[$our_form_values['format']];

  // TODO - maybe we should pass more context back at this point..
  // The parser may need to know the source and additional form settings.
  $parsed_result = importer_ui_run_parser_on_data($file_content, $format_info);

  if ($parsed_result) {
    $form_state['storage']['parsed_result'] = $parsed_result;
  }
  else {
    drupal_set_message(t('Ran parser over URL !file_path but got no result back', $strings), 'notice');
  }

}

/**
 * Check that a valid URL was given.
 */
function importer_ui_web_url_form_validate($form, &$form_state) {
  $service_id = $form_state['values']['service_id'];
  $our_form_values = $form_state['values'][$service_id];
  if (empty($our_form_values['url'])) {
    form_set_error("$service_id][url", t('Please enter the URL of a valid data source'));
  }
}
