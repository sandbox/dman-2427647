<?php
/**
 * @file
 * An importer service plugin definition.
 */

/**
 * Declare this plugin.
 */
$plugin = array(
  'provider'             => 'Local file',
  'name'                 => 'Local file',
  'id'                   => 'file_path',
  'description'          => "
    Enter the location of a local resource, relative to the Drupal site root.
  ",
  // Define the name of the form function that returns service-specific UI.
  'import_form_callback' => 'importer_ui_file_path_form',
  // The function name that will return the raw data - really easy this one!
  'request_callback'     => 'file_get_contents',
);


/**
 * A subform for entering the file path and format to be imported.
 *
 * Callback named in our hook_importer_ui_services
 *
 * $config will include the name of a callback we can use to retrieve the
 * supported formats
 *
 * Implements HOOK_SERVICE_form().
 */
function importer_ui_file_path_form(&$form_state, $service_info, $config) {

  // I need to be sure that this inc always gets loaded if using this form.
  // The automatic includes are failing.
  form_load_include($form_state, 'inc', 'importer_ui', 'importer_services/' . pathinfo(__FILE__, PATHINFO_FILENAME));

  $service_id = $service_info['id'];
  $our_form_values = @$form_state['input'][$service_id];

  $file_path = isset($our_form_values['file_path']) ? $our_form_values['file_path'] : variable_get('importer_ui_file_path', '');

  $formats_info = $config['formats_callback']();
  $formats_options = $config['formats_callback']('options');

  $form = array(
    '#type'  => 'fieldset',
    '#title' => $service_info['name'],
  );
  $form[$service_id] = array(
    '#type'              => 'textfield',
    '#title'             => t('File to import from'),
    '#description'       => t('
      Enter the path of a file on the server containing the data.
    '),
    '#default_value'     => $file_path,
    '#autocomplete_path' => 'admin/importer_ui/filepath_autocomplete',
    '#size'              => 128,
  );

  $form['format'] = array(
    '#type'          => 'select',
    '#title'         => t('Format of file'),
    '#default_value' => isset($our_form_values['format']) ? $our_form_values['format'] : variable_get('importer_ui_format', ''),
    '#options'       => $formats_options,
  );
  // Show additional optional descriptive text under the selected format.
  // Only reveal when the appropriate format is selected.
  foreach ($formats_info as $format_key => $format_info) {
    if (!empty($format_info['description'])) {
      $form['format_description'][$format_key] = array(
        '#type'       => 'item',
        'description' => array(
          '#markup' => $format_info['description'],
        ),
        '#states'     => array(
          'visible' => array(
            ':input[name="' . $service_id . '[format]"]' => array('value' => $format_key),
          ),
        ),
      );
    }
  }
  // TODO Additional format options?
  // A format may want to define its own callback form?

  $form['load_url'] = array(
    '#id'       => $service_id,
    '#type'     => 'submit',
    '#value'    => t('Load file'),
    '#validate' => array(
      'importer_ui_file_path_form_validate',
      'importer_ui_import_form_validate',
    ),
    '#submit'   => array('importer_ui_file_path_form_submit'),
  );

  // dpm(get_defined_vars(), __FUNCTION__);
  return $form;
}


/**
 * Check that the given filepath is accessible.
 */
function importer_ui_file_path_form_validate($form, &$form_state) {
  $service_id = $form_state['values']['service_id'];
  $our_form_values = $form_state['values'][$service_id];
  if (empty($our_form_values['file_path'])) {
    form_set_error("$service_id][file_path", t('Please enter the path of a valid data source'));
    return;
  }
  // Remember current prefs between times, just for convenience.
  if (isset($our_form_values['file_path'])) {
    variable_set('importer_ui_file_path', $our_form_values['file_path']);
  }
}

/**
 * Triggered when 'load file' is clicked.
 *
 * Loads and parses the file, stores results into form memory.
 *
 * @param array $form
 *   FAPI form array.
 * @param array $form_state
 *   FAPI form state.
 */
function importer_ui_file_path_form_submit($form, &$form_state) {
  // I think this button is SURE to be the one that is clicked by the
  // time we get here.
  if ($form_state['clicked_button']['#id'] != 'file_path') {
    return;
  }

  $form_state['rebuild'] = TRUE;
  // $config was stored for us at form create time.
  // $config = $form_state['importer_ui_config'];
  // We are part of a bigger form, and those are all the $form_values.
  $form_values = @$form_state['input'];
  // The clicked button defines the service id. eg 'file_path'
  $service_id = $form_state['clicked_button']['#id'];
  // And the service ID defines which part of the form we read and process.
  $our_form_values = @$form_values[$service_id];

  $strings = array(
    '!file_path' => l($our_form_values['file_path'], $our_form_values['file_path'])
  );

  // Early exits here is anything goes wrong.
  // Fetch the remote file.
  if (!is_file($our_form_values['file_path'])) {
    drupal_set_message(t("!file_path is not a file.", $strings), 'error');
    return;
  }
  $file_content = file_get_contents($our_form_values['file_path']);
  if (empty($file_content)) {
    drupal_set_message(t("Failed to retrieve content from file @file_path", $strings), 'error');
    return;
  }

  drupal_set_message("Parsing file and saving into form storage as parsed_result. (rebuilding form, found a file)");
  $formats_info = $config['formats_callback']();
  $format_info = $formats_info[$our_form_values['format']];
  $parsed_result = importer_ui_run_parser_on_data($file_content, $format_info);
  if ($parsed_result) {
    $form_state['storage']['parsed_result'] = $parsed_result;
  }
  else {
    drupal_set_message(t('Ran parser over !file_path but got no result back', $strings), 'notice');
  }
}


/**
 * Find the files they are looking for under the given path.
 *
 * Autocomplete for files on the current filesystem.
 *
 * A JSON FAPI callback from hook_menu.
 */
function importer_ui_filepath_autocomplete($search) {
  $search = implode('/', func_get_args());
  $file_paths = array();
  if (is_dir($search)) {
    $base = $search;
    $stem = '';
  }
  else {
    $base = dirname($search);
    $stem = basename($search);
  }
  // If the given filepath is a folder, list the contents.
  if (is_dir($base)) {
    $file_paths = array();
    $files = file_scan_directory($base, "/$stem.*/", array(
      'callback' => NULL,
      'recurse'  => FALSE,
    ));
    foreach ($files as $file) {
      $file_paths[$file->uri] = $file->filename;
    }
  }
  drupal_json_output($file_paths);
  return NULL;
}
