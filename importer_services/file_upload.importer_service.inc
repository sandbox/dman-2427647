<?php
/**
 * @file
 * An importer service plugin definition.
 */

/**
 * Declare this plugin.
 */
$plugin = array(
  'provider'             => 'File Upload',
  'name'                 => 'Direct File Upload',
  'id'                   => 'file_upload',
  'description'          => 'Upload a file in one of the supported formats.',
  // Define the name of the form function that returns service-specific UI.
  'import_form_callback' => 'importer_ui_file_upload_form',
);

/**
 * Return a subform allowing a user to upload data file to import.
 *
 * Callback named in our hook_importer_ui_services
 *
 * $config will include the name of a callback we can use to retrieve the
 * supported formats
 *
 * Implements HOOK_SERVICE_form().
 */
function importer_ui_file_upload_form(&$form_state, $service_info, $config) {
  // I need to be sure that this inc always gets loaded if using this form.
  // The automatic includes are failing.
  form_load_include($form_state, 'inc', 'importer_ui', 'importer_services/' . pathinfo(__FILE__, PATHINFO_FILENAME));

  $service_id = $service_info['id'];
  $our_form_values = @$form_state['values'][$service_id];

  $formats_info = $config['formats_callback']();
  $formats_options = $config['formats_callback']('options');

  $form = array(
    '#type'  => 'fieldset',
    '#title' => $service_info['name'],
  );
  $form['file_upload'] = array(
    '#type'        => 'file',
    '#title'       => t('File to import'),
    '#description' => t('If file upload fails, you may need to have the appropriate file extension added in the !upload_settings_link settings.', array('!upload_settings_link' => l(t('File uploads'), 'admin/config/uploads'))),
  );
  $form['format'] = array(
    '#type'          => 'select',
    '#title'         => t('Format of file'),
    '#default_value' => isset($our_form_values['format']) ? $our_form_values['format'] : variable_get('importer_ui_format', ''),
    '#options'       => $formats_options,
    '#description'   => t("Be sure to only upload valid files in the supported format."),
  );
  // Show additional optional descriptive text under the selected format.
  // Only reveal when the appropriate format is selected.
  foreach ($formats_info as $format_key => $format_info) {
    if (!empty($format_info['description'])) {
      $form['format_description'][$format_key] = array(
        '#type'       => 'item',
        'description' => array(
          '#markup' => $format_info['description'],
        ),
        '#states'     => array(
          'visible' => array(
            ':input[name="' . $service_id . '[format]"]' => array('value' => $format_key),
          ),
        ),
      );
    }
  }

  $form['upload_the_file'] = array(
    '#type'     => 'submit',
    '#value'    => t('Upload file'),
    '#validate' => array(
      'importer_ui_file_upload_form_validate',
      'importer_ui_import_form_validate'
    ),
    '#submit'   => array('importer_ui_file_upload_form_submit'),
    '#id'       => $service_id,
  );
  return $form;
}

/**
 * Submit handler triggered when 'load file' is clicked.
 *
 * Loads and parses the file, stores results into form memory.
 *
 * Implements HOOK_SERVICE_form_submit()
 */
function importer_ui_file_upload_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  // $config = $form_state['importer_ui_config'];
  $service_id = $form_state['clicked_button']['#id'];
  $form_values = @$form_state['input'];
  $our_form_values = @$form_values[$service_id];

  // Early exits here is anything goes wrong.
  // Security: NO VALIDATORS here so dodgy file types are allowed.
  // However, it's loaded into temp space .. and PHP code is explicitly
  // executed (!) anyway - if you have permissions.
  if (!$file = file_save_upload('file_upload', array('file_validate_extensions' => array()))) {
    drupal_set_message(t("File upload failed"), 'error');
    return;
  }
  if (!$file_content = file_get_contents($file->uri)) {
    drupal_set_message(t("Failed to read content from file"), 'error');
    return;
  }

  $formats_info = $config['formats_callback']();
  $format_info = $formats_info[$our_form_values['format']];
  $parsed_result = importer_ui_run_parser_on_data($file_content, $format_info);

  if ($parsed_result) {
    $form_state['storage']['parsed_result'] = $parsed_result;
    drupal_set_message('got something');
  }
  else {
    drupal_set_message(t('Ran parser over uploaded file but got no result back'), 'notice');
  }
}

/**
 * Check that the upload was successful.
 */
function importer_ui_file_upload_form_validate($form, &$form_state) {
  // $service_id = $form_state['values']['service_id'];
  // $our_form_values = $form_state['values'][$service_id];
  // TODO Should process/save file upload here.
  // dpm(get_defined_vars());
}
