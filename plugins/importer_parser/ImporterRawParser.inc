<?php
/**
 * @file
 * An importer service plugin definition.
 */

/**
 * Declare this plugin.
 */
$plugin = array(
  'name'        => 'Raw',
  'id'          => 'ImporterRawParser',
  'description' => "No processing. The data is available as 'raw'.",
  'handler'     => array(
    'class' => 'ImporterRawParser',
    'file'  => basename(__FILE__),
    'path'  => drupal_get_path('module', 'importer_ui') . '/plugins/importer_parser',
  ),
);

/**
 * Parses given input.
 *
 * VERY MUCH LIKE FeedsParser but only implements a little of it.
 */
class ImporterRawParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    $source_config = $source->getConfigFor($this);

    // Create a result object and return it.
    # return new FeedsParserResult($rows, $source->feed_nid);
  }
}
