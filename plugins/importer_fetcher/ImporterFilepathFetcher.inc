<?php
/**
 * @file
 * An importer service plugin definition.
 */

/**
 * Declare this plugin.
 */
$plugin = array(
  'name'                 => 'Local file',
  'id'                   => 'ImporterFilepathFetcher',
  'description'          => "
    Enter the location of a local resource, relative to the Drupal site root.
  ",
  // Define the name of the form function that returns service-specific UI.
  'import_form_callback' => 'importer_ui_file_path_form',
  // The function name that will return the raw data - really easy this one!
  'request_callback'     => 'file_get_contents',
  // Declare that this plugin is best handled by this class,
  // not just callbacks.
  'handler'              => array(
    #'parent' => 'FeedsFetcher',
    'class' => 'ImporterFilepathFetcher',
    'file'  => basename(__FILE__),
    'path'  => drupal_get_path('module', 'importer_ui') . '/plugins/importer_fetcher',
  ),

);


/**
 * Check that the given filepath is accessible.
 */
function xx_importer_ui_file_path_form_validate($form, &$form_state) {
  $service_id = $form_state['values']['service_id'];
  $our_form_values = $form_state['values'][$service_id];
  if (empty($our_form_values['file_path'])) {
    form_set_error("$service_id][file_path", t('Please enter the path of a valid data source'));
    return;
  }
  // Remember current prefs between times, just for convenience.
  if (isset($our_form_values['file_path'])) {
    variable_set('importer_ui_file_path', $our_form_values['file_path']);
  }
}

/**
 * Triggered when 'load file' is clicked.
 *
 * Loads and parses the file, stores results into form memory.
 *
 * @param array $form
 *   FAPI form array.
 * @param array $form_state
 *   FAPI form state.
 */
function xx_importer_ui_file_path_form_submit($form, &$form_state) {
  // I think this button is SURE to be the one that is clicked by the
  // time we get here.
  if ($form_state['clicked_button']['#id'] != 'file_path') {
    return;
  }

  $form_state['rebuild'] = TRUE;
  // $config was stored for us at form create time.
  // $config = $form_state['importer_ui_config'];
  // We are part of a bigger form, and those are all the $form_values.
  $form_values = @$form_state['input'];
  // The clicked button defines the service id. eg 'file_path'
  $service_id = $form_state['clicked_button']['#id'];
  // And the service ID defines which part of the form we read and process.
  $our_form_values = @$form_values[$service_id];

  $strings = array(
    '!file_path' => l($our_form_values['file_path'], $our_form_values['file_path'])
  );

  // Early exits here is anything goes wrong.
  // Fetch the remote file.
  if (!is_file($our_form_values['file_path'])) {
    drupal_set_message(t("!file_path is not a file.", $strings), 'error');
    return;
  }
  $file_content = file_get_contents($our_form_values['file_path']);
  if (empty($file_content)) {
    drupal_set_message(t("Failed to retrieve content from file @file_path", $strings), 'error');
    return;
  }

  drupal_set_message("Parsing file and saving into form storage as parsed_result. (rebuilding form, found a file)");
  $formats_info = $config['formats_callback']();
  $format_info = $formats_info[$our_form_values['format']];
  $parsed_result = importer_ui_run_parser_on_data($file_content, $format_info);
  if ($parsed_result) {
    $form_state['storage']['parsed_result'] = $parsed_result;
  }
  else {
    drupal_set_message(t('Ran parser over !file_path but got no result back', $strings), 'notice');
  }
}


/**
 * Find the files they are looking for under the given path.
 *
 * Autocomplete for files on the current filesystem.
 *
 * A JSON FAPI callback from hook_menu.
 */
function xx_importer_ui_filepath_autocomplete($search) {
  $search = implode('/', func_get_args());
  $file_paths = array();
  if (is_dir($search)) {
    $base = $search;
    $stem = '';
  }
  else {
    $base = dirname($search);
    $stem = basename($search);
  }
  // If the given filepath is a folder, list the contents.
  if (is_dir($base)) {
    $file_paths = array();
    $files = file_scan_directory($base, "/$stem.*/", array(
      'callback' => NULL,
      'recurse'  => FALSE,
    ));
    foreach ($files as $file) {
      $file_paths[$file->uri] = $file->filename;
    }
  }
  drupal_json_output($file_paths);
  return NULL;
}


///////////////////////////


/**
 * Fetches data from local filesystem.
 *
 * Though this is shaped a lot like a FeedsFetcher, it is much compressed.
 *
 * Differences include: there is no distinction between a configForm
 * (admin settings) and a sourceForm (request settings) as config and admin
 * happen in the same form when using importer_ui.
 * Maybe configs should be remembered globally or something, but lets just
 * skip the idea of advanced configs anyway.
 * - so there is no configForm()
 *
 * This would like to extend FeedsFetcher, but that has so much dependence on
 * FeedsConfigurable and other contextual things it's really hard to extricate.
 * Instead, imagine this as a FeedsFetcher that can exist standalone, and
 * offers a subset of those exected functions. Named the same for convenience.
 */
class ImporterFilepathFetcher {

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array( // 'request_timeout' => NULL,
    );
  }

  /**
   * A subform for entering the file path and format to be imported.
   *
   * @param array $config
   *   The local configs used to pre-seed the values. I do not know anything
   * about the rest of the context.
   */
  public function sourceForm($source_config) {

    $form = array();
    $form['source'] = array(
      '#type'              => 'textfield',
      '#title'             => t('File to import from'),
      '#description'       => t('
        Enter the path of a file on the server containing the data.
      '),
      '#default_value'     => isset($source_config['source']) ? $source_config['source'] : '',
      '#autocomplete_path' => 'admin/importer_ui/filepath_autocomplete',
      '#maxlength'         => NULL,
      '#required'          => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $values['source'] = trim($values['source']);

    // Keep a copy for error messages.
    $original_url = $values['source'];

    $parts = parse_url($values['source']);
    if (empty($parts['scheme']) && $this->config['auto_scheme']) {
      $values['source'] = $this->config['auto_scheme'] . '://' . $values['source'];
    }

    if (!feeds_valid_url($values['source'], TRUE)) {
      $form_key = 'feeds][' . get_class($this) . '][source';
      form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $original_url)));
    }
    elseif ($this->config['auto_detect_feeds']) {
      feeds_include_library('http_request.inc', 'http_request');
      if ($url = http_request_get_common_syndication($values['source'])) {
        $values['source'] = $url;
      }
    }
  }


  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    if ($this->config['use_pubsubhubbub'] && ($raw = $this->subscriber($source->feed_nid)
        ->receive())
    ) {
      return new FeedsFetcherResult($raw);
    }
    $fetcher_result = new FeedsHTTPFetcherResult($source_config['source']);
    // When request_timeout is empty, the global value is used.
    $fetcher_result->setTimeout($this->config['request_timeout']);
    return $fetcher_result;
  }

  /**
   * Clear caches.
   */
  public function clear(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = $source_config['source'];
    feeds_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Implements FeedsFetcher::request().
   */
  public function request($feed_nid = 0) {
    feeds_dbg($_GET);
    @feeds_dbg(file_get_contents('php://input'));
    // A subscription verification has been sent, verify.
    if (isset($_GET['hub_challenge'])) {
      $this->subscriber($feed_nid)->verifyRequest();
    }
    // No subscription notification has ben sent, we are being notified.
    else {
      try {
        feeds_source($this->id, $feed_nid)->existing()->import();
      } catch (Exception $e) {
        // In case of an error, respond with a 503 Service (temporary) unavailable.
        header('HTTP/1.1 503 "Not Found"', NULL, 503);
        drupal_exit();
      }
    }
    // Will generate the default 200 response.
    header('HTTP/1.1 200 "OK"', NULL, 200);
    drupal_exit();
  }

}
