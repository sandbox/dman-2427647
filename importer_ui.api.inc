<?php
/**
 * @file
 * Template for the functins we use internally as callbacks or pseudo hooks.
 */

/**
 * Return a list of file formats we consume, and the callbacks we use for them.
 */
function hook_importer_formats() {
  $formats = array(
    'xsd' => array(
      'id'                 => 'xsd',
      // Required for info.
      'name'               => 'XSD Schema Document',
      // Displayed in the selector.
      'module'             => 'xsd2cck',
      'parser_callback'    => 'xsd2cck_analyze_file_for_cck',
      // The parser_callback translates the format into a set of results.
      // It is not expected to actually process or make the changes,
      // but it analyzes it, reports on results, and returns a list of entities.
      'processor_callback' => 'xsd2cck_xsd_to_cck_content_type',
      // The processor callback may either import immediately, or offer more
      // choices - especially if there is more than one result.
      // It returns a form element - possilby just a message.
      'examples'           => array(
        'W3C Schema Testsuite' => 'http://dev.w3.org/cvsweb/~checkout~/XML/xml-schema-test-suite/2004-01-14/xmlschema2006-11-06/nistMeta/AnnotatedTSSchema.xsd?rev=1.5&content-type=text/plain',
      ),
    ),
  );
  return $formats;
}

/**
 * Returns a form for entering the named importer service details.
 *
 * @param array $form_state
 *   FAPI array.
 * @param array $service_info
 *   About the current service
 * @param array $config
 *   will include the name of a callback we can use to retrieve the
 *   supported formats if needed.
 */
function HOOK_SERVICE_form(&$form_state, $service_info, $config) {

}

/**
 * Submit handler triggered when 'load file' is clicked.
 *
 * Loads and parses the file, stores results into form memory.
 *
 * @param array $form
 *   FAPI form array.
 * @param array $form_state
 *   FAPI form state.
 */
function HOOK_SERVICE_form_submit($form, &$form_state) {
}
