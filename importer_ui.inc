<?php
/**
 * @file
 * UI form elements to support importing data from multiple sources.
 *
 * A library of FormAPI stuff that abstracts some form elements and selectors.
 *
 * The process is a multistep form that allows you to
 * - Select an input type
 * - Select the input source itself
 *   (the selection is different for different sources)
 * - Select the input format
 * - Preview the import results and change elements
 * - Perform the import.
 *
 * @author 'dman' Dan Morrison http://coders.co.nz/
 * @version 2010
 */


/**
 * The main import form page.
 *
 * This contains various sub-form elements that are delegated to further form
 * functions.
 * The further functions are retrieved by 'service' and 'format' lookups.
 *
 * Basic 'services' start with file upload and web URL.
 *
 * 'formats' may be like XML or CSV.
 *
 * The form includes ALL fields when built, but hides most of it using
 * Drupal 'states' to only show relevant parts.
 *
 * @param array $form_state
 *   FAPI
 * @param array $config
 *   Must contain values for
 *   'services_callback' and 'formats_callback'
 *   These are names of functions that describe the options.
 *
 * @return array
 *   FAPI subform.
 */
function importer_ui_import_form(&$form_state, $config) {
  $form = array();
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form_state["rebuild"] = TRUE;
  // Save this info so all deeper processes can find it.
  if (!isset($form_state['importer_ui_config'])) {
    $form_state['importer_ui_config'] = $config;
  }
  // I need to be sure that this inc always gets loaded if using this form.
  form_load_include($form_state, 'inc', 'importer_ui', 'importer_ui');

  // This form is always actually in place of another name, so the autoload
  // of the validate function by name won't work.
  // Name our handler, it does persistence, but not processing.
  $form['#validate'] = array('importer_ui_import_form_validate');

  // This form allows the input from different types of source,
  $form['fetcher_configs'] = importer_ui_fetcher_form($form_state, $config);

  // And different formats.
  $form['parser_configs'] = importer_ui_parser_form($form_state, $config);


  // $service_specific_form_callback($form_state, $fetcher_info, $config);

  /*

      if (isset($fetcher_info['import_form_callback'])) {
        $service_specific_form_callback = $fetcher_info['import_form_callback'];
        if (!function_exists($service_specific_form_callback)) {
          drupal_set_message(t('
            Invalid data source retrieval service function callback defined
            for %fetcher_id. Function %function was not available.',
              array(
                '%fetcher_id' => $fetcher_id,
                '%function'   => $service_specific_form_callback,
              )),
            'error'
          );
          continue;
        }
        $form['fetcher_configs'][$fetcher_id] = $service_specific_form_callback($form_state, $fetcher_info, $config);

        // Hide the subform until the selector has selected it.
        $form['fetcher_configs'][$fetcher_id]['#states'] = array(
          'visible' => array(
            ':input[name="fetcher_id"]' => array('value' => $fetcher_id),
          ),
        );

        $form['fetcher_configs'][$fetcher_id]['#tree'] = TRUE;
        // Each subset from here is in its own data array.
      }
      else {
        // No settings set for $fetcher_id
      }

      */


  dpm(get_defined_vars());

  // If one of the services has populated the 'parsed_result' by now though,
  // show a preview. Previewing may be optional though.
  if (isset($form_state['storage']['parsed_result'])) {
    $form['fetcher_configs']['#collapsible'] = TRUE;
    $form['fetcher_configs']['#collapsed'] = TRUE;
    $parsed_data = $form_state['storage']['parsed_result'];
    if (isset($config['preview_callback'])) {
      $preview_callback = $config['preview_callback'];
      if (!function_exists($preview_callback)) {
        drupal_set_message('Could not preview using ' . $preview_callback);
      }
      $form['preview'] = $preview_callback($form_state, $parsed_data);
    }
  }

  return $form;
}

/**
 * Provides a little persistence.
 *
 * MAY BE unneccessary
 * This does very little. service-specific submit handlers do the real work.
 *
 * THIS DOES NOT GET CALLED if you set a button-specific #validate function,
 * so if you want it, the sub-forms should invoke it themselves.
 * https://drupal.org/node/372818
 */
function importer_ui_import_form_validate($form, &$form_state) {
  // Remember current prefs between times, just for convenience.
  variable_set('importer_ui_fetcher_id', $form_state['values']['fetcher_id']);
}

function importer_ui_fetcher_form(&$form_state, $config) {

  // Each importer service may be loaded from external modules.
  // Call the callback functions listed in the configs.
  if (empty($config['fetchers'])) {
    if (isset($config['fetchers_callback']) && function_exists($config['fetchers_callback'])) {
      $config['fetchers'] = $config['fetchers_callback']();
    }
    else {
      $config['fetchers'] = importer_ui_all_plugins('fetcher');
    }
  }
  // Prepare list of labels for the option selector.
  $fetchers_list = importer_ui_plugin_options_list($config['fetchers']);

  $form = array(
    '#type'       => 'fieldset',
    '#attributes' => array(
      'id' => 'fetcher_configs',
    ),
    '#title'      => t('Data Source'),
  );
  $fetcher_id = isset($form_state['values']['fetcher_id']) ? $form_state['values']['fetcher_id'] : variable_get('importer_ui_fetcher_id', 'none');
  $form['fetcher_id'] = array(
    '#type'          => 'select',
    '#options'       => $fetchers_list,
    '#id'            => 'fetcher_id',
    '#default_value' => $fetcher_id,
    '#description'   => t('Select a source or service to pull the input from.'),
  );

  // Display each available data source service, and let them present their
  // own forms.
  foreach ($config['fetchers'] as $plugin_id => $plugin_info) {
    // Individual services may insert their own buttons and UI here.

    $subform = array(
      '#type'   => 'fieldset',
      '#title'  => $plugin_info['name'],
      '#states' => array(
        'visible' => array(
          ':input[name="fetcher_id"]' => array('value' => $plugin_id),
        ),
      ),
    );

    $subform['description'] = array(
      '#type'  => 'html_tag',
      '#tag'   => 'p',
      '#value' => $plugin_info['description'],
    );

    // Call the fetchers form function.

    // Feeds already has an API wrapper for this.
    // feeds_get_form(), but that's no good, it prepares the whole
    // thing. We just want the form array.

    // Pretend it's a FeedsFetcher, but skip most of the auto-config magic.
    $plugin = importer_ui_instantiate($plugin_id, $plugin_info, $config);

    if (method_exists($plugin, 'sourceForm')) {
      // sourceForm expects an array of configs relevant to itself.
      // If our global configs know that, pass it in.
      $subform_config = @$config['fetcher_configs'][$plugin_id];
      $subform += $plugin->sourceForm($subform_config);
    }

    $form[$plugin_id] = $subform;
  }

  return $form;
}


function importer_ui_parser_form(&$form_state, $config) {

  // Each importer service may be loaded from external modules.
  // Call the callback functions listed in the configs.
  if (empty($config['parserss'])) {
    if (isset($config['parsers_callback']) && function_exists($config['parsers_callback'])) {
      $config['parsers'] = $config['parsers_callback']();
    }
    else {
      $config['parsers'] = importer_ui_all_plugins('parser');
    }
  }
  // Prepare list of labels for the option selector.
  $plugins_list = importer_ui_plugin_options_list($config['parsers']);

  $form = array(
    '#type'       => 'fieldset',
    '#attributes' => array(
      'id' => 'parser_configs',
    ),
    '#title'      => t('Data Parser'),
  );
  $parser_id = isset($form_state['values']['parser_id']) ? $form_state['values']['parser_id'] : variable_get('importer_ui_parser_id', 'none');
  $form['parser_id'] = array(
    '#type'          => 'select',
    '#options'       => $plugins_list,
    '#id'            => 'parser_id',
    '#default_value' => $parser_id,
    '#description'   => t('Select the expected format.'),
    '#weight'        => -1,
  );

  // Display each available data source service, and let them present their
  // own forms.
  foreach ($config['parsers'] as $plugin_id => $plugin_info) {
    // Individual services may insert their own buttons and UI here.

    $subform = array(
      '#type'   => 'fieldset',
      '#title'  => $plugin_info['name'],
      '#states' => array(
        'visible' => array(
          ':input[name="parser_id"]' => array('value' => $plugin_id),
        ),
      ),
      // Because ?? one plugin (CSV parser) decided to set its own weight.
      // Nope.
      '#weight' => NULL,
    );

    $subform['description'] = array(
      '#type'  => 'html_tag',
      '#tag'   => 'p',
      '#value' => $plugin_info['description'],
    );

    // Call the fetchers form function.

    // Feeds already has an API wrapper for this.
    // feeds_get_form(), but that's no good, it prepares the whole
    // thing. We just want the form array.

    // Pretend it's a FeedsFetcher, but skip most of the auto-config magic.
    $plugin = importer_ui_instantiate($plugin_id, $plugin_info, $config);

    if (method_exists($plugin, 'sourceForm')) {
      // sourceForm expects an array of configs relevant to itself.
      // If our global configs know that, pass it in.
      $subform_config = @$config['parser_configs'][$plugin_id];
      $subform += $plugin->sourceForm($subform_config);
    }

    $form[$plugin_id] = $subform;
  }

  return $form;
}


/**
 * Response to my own hook_cck_importer_services.
 *
 * Return a list of ways and sources I can import data from.
 */
function importer_ui_basic_importer_services($mode = 'full') {
  // TODO cache?

  // Scan my own 'services' directory.
  $services = array();
  $plugins_dir = drupal_get_path('module', 'importer_ui') . '/importer_services';
  $includes = file_scan_directory($plugins_dir, '/.*.importer_service.inc/');
  foreach ($includes as $inc) {
    // Include any include files I find there.
    // Add their definition to my list of services.
    $plugin = NULL;
    require_once $inc->uri;
    if (!empty($plugin)) {
      $services[$plugin['id']] = $plugin;
    }
  }

  if ($mode == 'options') {
    // Return an array suitable for use in a form select element.
    $options = array();
    foreach ($services as $id => $service) {
      $options[$id] = $service['provider'] . " - " . $service['name'];
    }
    return $options;
  }

  return $services;
}

/**
 * Run the named functions on the now-available data.
 *
 * Return the parsed struct.
 */
function importer_ui_run_parser_on_data($file_content, $format_info) {
  // Run the parser on the available data.
  if (empty($file_content)) {
    drupal_set_message(t("No file content."), 'error');
    return FALSE;
  }

  if (empty($format_info['parser_callback'])) {
    drupal_set_message(t("Unable to run any callback parser function for %module. Undefined parser_callback for format %name.", array(
      '%module' => $format_info['module'],
      '%name'   => $format_info['name'],
    )), 'error');
    return FALSE;
  }
  $parser_func = $format_info['parser_callback'];
  if (!function_exists($parser_func)) {
    drupal_set_message(t("Unable to run named callback parser function @parser_func named by %module. Function does not exist", array(
      '@parser_func' => $parser_func,
      '%module'      => $format_info['module'],
    )), 'error');
    return FALSE;
  }

  drupal_set_message("Running $parser_func");
  $parsed_result = $parser_func($file_content);

  // The parser is done. Probably returned an array of parsed results.

  if (empty($parsed_result)) {
    drupal_set_message(t("Ran the parser @parser_func but got no results.", array('@parser_func' => $parser_func)));
    return NULL;
  }
  else {
    // Summarize that parsing was OK, and return the results;
    drupal_set_message(t("Parsed OK"));
    return $parsed_result;
  }
}

/**
 * Runs the import process, returns a renderable explaining how that went.
 *
 * @param array $data_object
 *   Info?
 *
 * @return bool|array
 *   Success.
 */
function UNUSED_importer_ui_run_processor_on_data($data_object) {
  if (isset($format_info['preview_callback'])) {
    $preview_func = $format_info['preview_callback'];
  }
  if (!empty($preview_func) && !function_exists($preview_func)) {
    drupal_set_message(t("Invalid preview func, could not find function %preview_func.", array('%preview_func' => $preview_func)), 'error');
    return FALSE;
  }

  // Now do the processing or execution of it.
  if (!$processor_func = @$format_info['processor_callback']) {
    drupal_set_message(t("No processor func for @format. The format was supposed to declare a 'processor_callback'.",
      array('@format' => $format_info['id'])
    ), 'error');
    return FALSE;
  }

  if (!function_exists($processor_func)) {
    drupal_set_message(t("Invalid processor func, could not find function %processor_func.", array('%processor_func' => $processor_func)), 'error');
    return FALSE;
  }
  $form = $processor_func($data_object);
  return $form;
}

/////////////////////////////

/**
 * Go through a list of fetchers and pull out their labels for an option list.
 *
 * @param $plugins
 */
function importer_ui_plugin_options_list($plugins) {
  $options = array();
  foreach ($plugins as $plugin_id => $plugin) {
    $options[$plugin_id] = $plugin['name'];
  }
  return $options;
}


/**
 * Return a list of all fetcher, both from feeds, and my ones.
 *
 * The can be called on to inspect and filter from the availables,
 * or just to return all available.
 */
function importer_ui_all_plugins($plugin_type) {

  $feeds_plugins = FeedsPlugin::byType($plugin_type);

  ctools_include('plugins');
  // Eg importer_fetcher, importer_parser.
  $my_plugins = ctools_get_plugins('importer_ui', 'importer_' . $plugin_type);

  return $feeds_plugins + $my_plugins;
}

/**
 * Load up an instance of the named class.
 *
 * AVOID using too much of the FeedsConfigurable overhead if we don't need it.
 * @see feeds_plugin()
 */
function importer_ui_instantiate($plugin_id, $plugin_info, $config) {
  // If it's a FeedsConfigurable, MUST use feeds api.
  if ($plugin_info['module'] == 'feeds') {
    $plugin = feeds_plugin($plugin_id, $plugin_id);
    return $plugin;
  }

  // Else do it the normal way. We have lots of info about it already.
  // In fact, is this load_class even needed?
  ctools_include('plugins');
  if ($class = ctools_plugin_load_class($plugin_info['plugin module'], $plugin_info['plugin type'], $plugin_id, 'handler')) {
    return new $class($plugin_id);
  }

  drupal_set_message(t('Unable to initialize plugin %plugin_id', array('%plugin_id' => $plugin_id)), 'error');
  debug($plugin_info);
  return NULL;
}
