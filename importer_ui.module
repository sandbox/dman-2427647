<?php
/**
 * @file
 * UI form elements to support importing data from multiple sources.
 *
 * A library of FormAPI stuff that abstracts some form elements and selectors.
 *
 * This is a utility to assist uploading, importing or absorbing resources
 * as part of a larger admin form.
 * Supported methods so far are:
 * - Direct file upload
 * - Direct URL entry
 * - Local file path
 * Selection of files from a designated folder
 *
 * Alternative sources (eg web service requests) can extend this list via hooks.
 *
 * The second phase is the processing phase
 * - how to interpret the retrieved data.
 *
 * This happens by running a chosen parser callback on the data,
 * depending on its chosen format.
 *
 * @author 'dman' Dan Morrison http://coders.co.nz/
 */

/**
 * Declare an autocomplete callback for the filesystem utility.
 *
 * Note security danger here, so require high level admin only.
 */
function importer_ui_menu() {
  return array(
    'admin/importer_ui/filepath_autocomplete' => array(
      'page callback'    => 'importer_ui_filepath_autocomplete',
      'file'             => 'importer_services/file_path.importer_service.inc',
      'page arguments'   => array(),
      'access arguments' => array('administer site configuration'),
      'type'             => MENU_CALLBACK,
    ),
  );
}

function importer_ui_include() {
  module_load_include('inc', 'importer_ui', 'importer_ui');
}


/**
 * Announce that we define plugins.
 *
 * Implements hook_ctools_plugin_type().
 */
function importer_ui_ctools_plugin_type() {
  return array(
    'importer_fetcher' => array(
      'cache'     => TRUE,
      'use hooks' => TRUE,
      'classes'   => array('handler'),
    ),
    'importer_parser'  => array(
      'cache'     => TRUE,
      'use hooks' => TRUE,
      'classes'   => array('handler'),
    ),
  );
}

/**
 * We load our own formats via plugins.
 *
 * Implements hook_ctools_plugin_directory().
 */
function importer_ui_ctools_plugin_directory($module, $plugin) {
  if ($module == 'importer_ui') {
    return 'plugins/' . $plugin;
  }
  return NULL;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function importer_ui_ctools_plugin_api($owner, $api) {
  if ($owner == 'importer_ui' && $api == 'importer_fetcher') {
    return array('version' => 1);
  }
  if ($owner == 'importer_ui' && $api == 'importer_parser') {
    return array('version' => 1);
  }
  return NULL;
}

